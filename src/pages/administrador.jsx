// import '../css/administrador.css'
import React, { useEffect, useState } from 'react';

export const Administrador = ({onFormSwitch}) => {
  const [textosRegistrados, setTextosRegistrados] = useState([]);
  useEffect(() => {
    const storedTextos = JSON.parse(localStorage.getItem('textosRegistrados')) || [];
    setTextosRegistrados(storedTextos);
  }, []);

  const registrarTexto = () => {
    const textoInput = document.getElementById('texto').value;
    const updatedTextos = [...textosRegistrados, textoInput];
    setTextosRegistrados(updatedTextos);
    localStorage.setItem('textosRegistrados', JSON.stringify(updatedTextos));
    document.getElementById('texto').value = '';
    actualizarLista();
  };

  const eliminarTexto = (index) => {
    const updatedTextos = [...textosRegistrados];
    updatedTextos.splice(index, 1);
    setTextosRegistrados(updatedTextos);
    localStorage.setItem('textosRegistrados', JSON.stringify(updatedTextos));
    actualizarLista();
  };
  const handleLogin1 = () => {
    onFormSwitch('login1');
  };
  const actualizarLista = () => {
    const listaTextos = document.getElementById('listaTextos');
    listaTextos.innerHTML = '';

    textosRegistrados.forEach((texto, i) => {
      const listItem = document.createElement('li');
      listItem.textContent = texto;

      const deleteButton = document.createElement('button');
      deleteButton.textContent = 'Eliminar';
      deleteButton.onclick = () => eliminarTexto(i);

      listItem.appendChild(deleteButton);
      listaTextos.appendChild(listItem);
    });
  };

  return (
    <div>
      <h1>Registro de planes</h1>
      <form id="registroForm">
        <label htmlFor="texto">Detalles:</label>
        <input type="text" id="texto" required />
        <button onClick={registrarTexto}>Registrar Plan</button>
      </form>
      <ul id="listaTextos"></ul>
      <button 
        type="button"
        onClick={handleLogin1}
      >
        Iniciar sesión como usuario
      </button>
    </div>
  );
};


