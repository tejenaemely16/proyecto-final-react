import React from 'react';
import '../css/index2.css'
import sede1 from '../img/ubicacion1.png'
import sede2 from '../img/ubicacion2.png'
// import sede3 from '../img/ubicacion3.png'
// import sede4 from '../img/ubicacion4.png'
export const GymComponent = ({onFormSwitch}) => {
  const handleIndex = () => {
    onFormSwitch('index');
  };
  const handlePlanes = () => {
      onFormSwitch('planes');
  };
  const handleSedes = () => {
      onFormSwitch('sedes');
  };
  const articlesData = [
    {
      titulo: 'Manta',
      descripcion: 'Puedes encontrar nuestra sede en la calle 13.',
      imagenSrc: require('../img/ubicacion1.png').default,
      imagenAlt: 'Imagen 1',
    },
    {
      titulo: 'Guayaquil',
      descripcion: 'Puedes encontrar nuestra sede en la calle 45.',
      imagenSrc: '../img/ubicacion2.apng',
      imagenAlt: 'Imagen 2',
    },
    {
      titulo: 'Cuenca',
      descripcion: 'Puedes encontrar nuestra sede en la calle 27.',
      imagenSrc: '../img/ubicacion3.png',
      imagenAlt: 'Imagen 1',
    },
    {
      titulo: 'Quito',
      descripcion: 'Puedes encontrar nuestra sede en la calle 36.',
      imagenSrc: '../img/ubicacion4.png',
      imagenAlt: 'Imagen 2',
    },
  ];

  return (
    <div>
      <header className="arriba">
        <div className="barra">
        <nav className="logo">
          <button
            type="button"
            onClick={handleIndex}
          >
            Gimnasio Tejena
          </button>
        </nav>
        <nav className="navegacion">
          <button
            type="button"
            onClick={handlePlanes}
          >
            Planes
          </button>
          <button
            type="button"
            onClick={handleSedes}
          >
            Sedes
          </button>
        </nav>
        </div>
      </header>

      <main className="medio">
        <section className="seccion2">
          {articlesData.slice(0, 2).map((article, index) => (
            <article key={index} className="articulos">
              <h2 className="titulo">{article.titulo}</h2>
              <p className="descripcion">{article.descripcion}</p>
              <img src={sede1} className="imagenArticulo" alt={article.imagenAlt} />
            </article>
          ))}
        </section>

        <section className="seccion2">
          {articlesData.slice(2, 4).map((article, index) => (
            <article key={index} className="articulos">
              <h2 className="titulo">{article.titulo}</h2>
              <p className="descripcion">{article.descripcion}</p>
              <img src={sede2} className="imagenArticulo" alt={article.imagenAlt} />
            </article>
          ))}
        </section>
      </main>

      <footer className="abajo">
        <div className="redes-sociales">
          {/* Código de redes sociales */}
        </div>
      </footer>
    </div>
  );
};


