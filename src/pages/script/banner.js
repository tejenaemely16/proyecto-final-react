var contenidoActual = 1;
var totalContenidos = 3;

document.addEventListener("DOMContentLoaded", function() {
    mostrarContenido();

    var botonAnterior = document.getElementById("anterior");
    var botonSiguiente = document.getElementById("siguiente");

    botonAnterior.addEventListener("click", function() {
        mostrarContenido('anterior');
    });

    botonSiguiente.addEventListener("click", function() {
        mostrarContenido('siguiente');
    });
});

function mostrarContenido(direccion) {
    var contenido = document.getElementById("contenido" + contenidoActual);
    contenido.style.display = "none";

    if (direccion === 'anterior') {
        contenidoActual = (contenidoActual - 1) <= 0 ? totalContenidos : contenidoActual - 1;
    } else {
        contenidoActual = (contenidoActual + 1) > totalContenidos ? 1 : contenidoActual + 1;
    }

    contenido = document.getElementById("contenido" + contenidoActual);
    contenido.style.display = "block";
}
