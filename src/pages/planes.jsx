import React, { useEffect, useState } from 'react';

export const Navegacion = ({onFormSwitch}) => {
  const handleIndex = () => {
    onFormSwitch('index');
  };
  const handlePlanes = () => {
      onFormSwitch('planes');
  };
  const handleSedes = () => {
      onFormSwitch('sedes');
  };
  const [textosRegistrados, setTextosRegistrados] = useState([]);

  useEffect(() => {
    actualizarLista();
  }, []);

  const actualizarLista = () => {
    const textos = JSON.parse(localStorage.getItem('textosRegistrados')) || [];
    setTextosRegistrados(textos);
  };

  return (
    <><div className="arriba">
      <div className="barra">
        <nav className="logo">
          <button
            type="button"
            onClick={handleIndex}
          >
            Gimnasio Tejena
          </button>
        </nav>
        <nav className="navegacion">
          <button
            type="button"
            onClick={handlePlanes}
          >
            Planes
          </button>
          <button
            type="button"
            onClick={handleSedes}
          >
            Sedes
          </button>
        </nav>
      </div>
      <h1>Lista de Servicios disponibles</h1>
      <ul id="listaTextos">
        {textosRegistrados.map((texto, index) => (
          <li key={index}>Plan {index + 1}: {texto}</li>
        ))}
      </ul>
    </div></>
  );
};


