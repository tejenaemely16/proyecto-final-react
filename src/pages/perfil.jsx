import React, { useEffect } from 'react';
import '../css/perfil.css';

export const UserProfile = ({ onFormSwitch }) => {
  const handleLogin1 = () => {
    onFormSwitch('login1');
  };
  useEffect(() => {
    const userProfile = document.getElementById("user-profile");

    // Obtener el botón de cerrar sesión dentro del useEffect
    const cerrarSesionButton = document.getElementById("cerrar-sesion");

    
    const handleInscripciones = () => {
      onFormSwitch('inscripciones');
    };

    // Obtener los datos del usuario desde localStorage
    const usuarios = JSON.parse(localStorage.getItem("usuarios"));
    const usuario = usuarios && usuarios.length > 0 ? usuarios[0] : null;

    if (usuario) {
      // Construir la estructura HTML para mostrar los datos del usuario
      const profileHTML = `
        <p><strong>Nombre de Usuario:</strong> ${usuario.username}</p>
        <p><strong>Nombres:</strong> ${usuario.nombres}</p>
        <p><strong>Apellidos:</strong> ${usuario.apellidos}</p>
        <p><strong>Correo Electrónico:</strong> ${usuario.correo}</p>
        <p><strong>Número de Celular:</strong> ${usuario.celular}</p>
        <p><strong>Edad:</strong> ${usuario.edad}</p>
      `;

      userProfile.innerHTML = profileHTML;
    }

    if (cerrarSesionButton) {
      cerrarSesionButton.addEventListener("click", handleInscripciones);
    }

    // Cleanup del event listener al desmontar el componente
    return () => {
      if (cerrarSesionButton) {
        cerrarSesionButton.removeEventListener("click", handleInscripciones);
      }
    };
  }, [onFormSwitch]);

  return (
    <div className="container">
      <h2>Mi Perfil</h2>
      <div id="user-profile">
        {/* Aquí se mostrarán los datos del usuario */}
      </div>
      {/* Botón para cerrar sesión */}
      <button type="button" id="cerrar-sesion">Inscribir servicios</button>
      <button
          type="button"
          onClick={handleLogin1}
        >
          Cerrar sesión
      </button>
    </div>
  );
};
