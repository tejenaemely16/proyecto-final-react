import React, { useState } from 'react';
//import '../css/login2.css';

export const AdminLogin = (props) => {
  const [useradmin, setUseradmin] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');

  const validarFormulario = () => {
    if (useradmin === 'admin' && password === '123') {
      showMessage('Inicio de sesión exitoso', 'success');
      props.onFormSwitch('administrador'); // Cambio a la vista de administrador
    } else {
      showMessage('Usuario o contraseña incorrectos. Por favor, inténtelo de nuevo.', 'error');
    }
  };

  const showMessage = (text, type) => {
    setMessage(text);
    if (type === 'success') {
      alert('Inicio de sesión exitoso.');
    } else if (type === 'error') {
      alert('Usuario o contraseña incorrectos. Por favor, inténtelo de nuevo.');
    }
  };

  const handleRegresarUsuario = () => {
    props.onFormSwitch('login1');
  };
  return (
    <div className="container">
      <h1>Administrador</h1>
      <form onSubmit={validarFormulario}>
        <label htmlFor="useradmin">Usuario:</label>
        <input
          type="text"
          id="useradmin"
          name="useradmin"
          required
          value={useradmin}
          onChange={(e) => setUseradmin(e.target.value)}
        /><br/><br/>
        <label htmlFor="password">Contraseña:</label>
        <input
          type="password"
          id="password"
          name="password"
          required
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        /><br/><br/>
        <input type="submit" value="Iniciar Sesión" />
      </form>
      <div id="message">{message}</div>
      <div className="buttons">
        <button
          type="button"
          onClick={handleRegresarUsuario}
        >
          Iniciar sesión como usuario
        </button>
      </div>
    </div>
  );
};
