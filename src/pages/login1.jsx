import React, { useState } from 'react';
//import '../css/login.css';

export const Login = (props) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [message, setMessage] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();

    // Validar las credenciales ingresadas
    const usuarios = JSON.parse(localStorage.getItem('usuarios')) || [];
    const usuarioEncontrado = usuarios.find(
      (user) => user.username === username && user.password === password
    );

    if (usuarioEncontrado) {
      showMessage('Inicio de sesión exitoso', 'success');
      // Redirigir al usuario a la página de perfil
      props.onFormSwitch('perfil');
    } else {
      showMessage('Credenciales incorrectas. Por favor, inténtelo nuevamente', 'error');
    }
  };

  const showMessage = (text, type) => {
    setMessage(text);
    if (type === 'success') {
      alert('Inicio de sesión exitoso.');
    } else if (type === 'error') {
      alert('Credenciales incorrectas. Por favor, inténtelo nuevamente.');
    }
  };

  const handleRegresarIndex = () => {
    props.onFormSwitch('index');
  };

  const handleRegresarRegistro = () => {
    props.onFormSwitch('registro');
  };

  const handleRegresarAdmin = () => {
    props.onFormSwitch('login2');
  };

  return (
    <div className="container">
      <h2>Iniciar Sesión</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="username">Nombre de usuario:</label>
          <input
            type="text"
            id="username"
            name="username"
            required
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Contraseña:</label>
          <input
            type="password"
            id="password"
            name="password"
            required
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button type="submit">Iniciar Sesión</button>
      </form>
      <div id="message">{message}</div>
      <div className="buttons">
        <button
          type="button"
          onClick={handleRegresarIndex}
        >
          Regresar a la pestaña principal
        </button>
        <button
          type="button"
          onClick={handleRegresarRegistro}
        >
          ¿No tienes cuenta? Regístrate
        </button>
        <button
          type="button"
          onClick={handleRegresarAdmin}
        >
          Iniciar sesión como administrador
        </button>
      </div>
    </div>
  );
};
