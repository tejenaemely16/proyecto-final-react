//import '../css/index2.css'
import articulo1 from '../img/articulo1.jpg'
import articulo2 from '../img/articulo2.jpg'
export const Index2 = ({ onFormSwitch }) => {
    const handleIndex = () => {
        onFormSwitch('index');
    };
    const handlePlanes = () => {
        onFormSwitch('planes');
    };
    const handleSedes = () => {
        onFormSwitch('sedes');
      };
    const handleLogin1 = () => {
        onFormSwitch('login1');
    };
    return (
      <>
      <div className="arriba">
            <div className="barra">
                <nav className="logo">
                <button
                    type="button"
                    onClick={handleIndex}
                    >
                    Gimnasio Tejena
                </button>
                </nav>
                <nav className="navegacion">
                <button
                    type="button"
                    onClick={handleSedes}
                    >
                    Sedes
                </button>
                <button
                    type="button"
                    onClick={handlePlanes}
                    >
                    Planes
                </button>
                </nav>
                <nav className="iniciarSesion">
                <button
                    type="button"
                    onClick={handleLogin1}
                    >
                    Iniciar Sesión
                </button>
                </nav>
            </div>
        </div><main className="medio">
                <section className="seccion1">
                    <article className="articulo1">
                        <h2 className="titulo1">Encuentra la sede más cercana a ti.</h2>
                        <div className="seccion1Banner">
                            <div className="seccion1BannerContenido">
                                <button
                                    type="button"
                                    onClick={handleSedes}
                                    >
                                    Sedes
                                </button>
                            </div>
                        </div>
                    </article>
                </section>
                <section className="seccion2">
                    <article className="articulos articulo2">
                        <h2 className="titulo2">Equipo de calidad</h2>
                        <p className="descripcion">Contamos con equipo de primer nivel para todos los clientes.</p>
                        {/* Reemplaza la ruta de la imagen con la ruta correcta en tu proyecto */}
                        <img src={articulo1} className="imagenArticulo" alt="Imagen 1" />
                    </article>
                    <article className="articulos articulo3">
                        <h2 className="titulo3">Entrenadores con experiencia.</h2>
                        <p className="descripcion">Nuestros Entrenadores están certificados y capacitados para realizar un buen desempeño en sus actividades.</p>
                        {/* Reemplaza la ruta de la imagen con la ruta correcta en tu proyecto */}
                        <img src={articulo2} className="imagenArticulo" alt="Imagen 2" />
                    </article>
                </section>
            </main><footer className="abajo">
                <div className="redes-sociales">
                    {/* <a href="https://www.facebook.com" target="_blank" className="social-icon">Facebook</a>
                    <a href="https://twitter.com" target="_blank" className="social-icon">Twitter</a> */}
                    {/* Puedes agregar más botones de redes sociales aquí */}
                </div>
            </footer>
            </>
    );
  };