// import '../css/activos.css'
import React, { useEffect, useState } from 'react';

export const ServiciosActivos = ({ onFormSwitch }) => {
  const [servicios, setServicios] = useState([]);

  useEffect(() => {
    const serviciosFromLocalStorage = JSON.parse(localStorage.getItem('servicios')) || [];
    setServicios(serviciosFromLocalStorage);
  }, []);

  const eliminarServicio = (index) => {
    const updatedServicios = [...servicios];
    updatedServicios.splice(index, 1);
    setServicios(updatedServicios);
    localStorage.setItem('servicios', JSON.stringify(updatedServicios));
  };
  const handleInscripciones = () => {
    onFormSwitch('inscripciones');
  };
  const handlePerfil = () => {
    onFormSwitch('perfil');
  };
  return (
    <div className="container">
      <h2>Servicios Registrados:</h2>
      <ul id="lista-servicios">
        {servicios.length === 0 ? (
          <p>No se han registrado ningún plan.</p>
        ) : (
          servicios.map((servicio, index) => (
            <li key={index}>
              <strong>Plan activo #{index + 1}:</strong> {servicio.seleccion}<br />
              <strong>Fecha y hora en la que se realizará el pago:</strong> {servicio.valor}<br />
              <strong>Duración del plan:</strong> {servicio.meses} meses<br />
              <button onClick={() => eliminarServicio(index)}>Eliminar</button>
            </li>
          ))
        )}
      </ul>
      <button
          type="button"
          onClick={handleInscripciones}
        >
          Regresar a la inscripciones
      </button>
      <button
          type="button"
          onClick={handlePerfil}
        >
          Regresar al perfil
      </button>
    </div>
  );
};

