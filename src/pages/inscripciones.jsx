import React, { useState } from 'react';

export const RegistroServicios = ({onFormSwitch}) => {
  const [opcionesServicio, setOpcionesServicio] = useState('seleccionar');
  const [fechaPago, setFechaPago] = useState('');
  const [horaPago, setHoraPago] = useState('');
  const [meses, setMeses] = useState('seleccionar');

  const handleSubmit = (e) => {
    e.preventDefault();

    if (opcionesServicio === 'seleccionar' || meses === 'seleccionar') {
      alert('Por favor, seleccione un servicio y una duración válidos.');
      return;
    }

    const valorSeleccionado = fechaPago + ' ' + horaPago;

    const servicios = JSON.parse(localStorage.getItem('servicios')) || [];
    const servicio = {
      seleccion: opcionesServicio,
      valor: valorSeleccionado,
      meses: meses,
    };

    servicios.push(servicio);
    localStorage.setItem('servicios', JSON.stringify(servicios));

    alert('Servicio registrado con éxito.');
    resetForm();
  };

  const resetForm = () => {
    setOpcionesServicio('seleccionar');
    setFechaPago('');
    setHoraPago('');
    setMeses('seleccionar');
  };

  const handleVerServicios = () => {
    onFormSwitch('activos');
  };

  const handleRegresarPerfil = () => {
    onFormSwitch('perfil');
  };

  return (
    <div className="container">
      <h2>Registro de Servicios</h2>
      <form onSubmit={handleSubmit} id="registro-servicios-form">
        <div className="form-group">
          <label htmlFor="opciones-servicio">Seleccione un servicio:</label>
          <select
            id="opciones-servicio"
            name="opciones-servicio"
            value={opcionesServicio}
            onChange={(e) => setOpcionesServicio(e.target.value)}
            required
          >
            <option value="seleccionar">Seleccionar un servicio:</option>
            <option value="Plan Gimnasio">Plan de gimnasio completo</option>
            <option value="Plan entrenador">Plan con entrenador</option>
            <option value="Plan alimentación">Plan de alimentación</option>
          </select>
        </div>
        <div className="form-group">
          <label htmlFor="fecha-pago">Fecha en la que cancelará valores en su sede:</label>
          <input
            type="date"
            id="fecha-pago"
            name="fecha-pago"
            value={fechaPago}
            onChange={(e) => setFechaPago(e.target.value)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="hora-pago">Hora en la que cancelará valores en su sede:</label>
          <input
            type="time"
            id="hora-pago"
            name="hora-pago"
            value={horaPago}
            onChange={(e) => setHoraPago(e.target.value)}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="meses">Duración en meses:</label>
          <select
            id="meses"
            name="meses"
            value={meses}
            onChange={(e) => setMeses(e.target.value)}
            required
          >
            <option value="seleccionar">Seleccionar la duración en meses.</option>
            {/* Opciones para los meses */}
            <option value="seleccionar">Seleccionar la duración en meses.</option>
            <option value="1">1 mes</option>
            <option value="2">2 meses</option>
            <option value="3">3 meses</option>
            <option value="4">4 meses</option>
            <option value="5">5 meses</option>
            <option value="6">6 meses</option>
            <option value="7">7 meses</option>
            <option value="8">8 meses</option>
            <option value="9">9 meses</option>
            <option value="10">10 meses</option>
            <option value="11">11 meses</option>
            <option value="12">12 meses</option>
            <option value="13">13 meses</option>
          </select>
        </div>
        <button type="submit">Registrar Servicio</button>
      </form>
      <button type="button" onClick={handleVerServicios}>
        Ver Servicios Registrados
      </button>
      <button type="button" onClick={handleRegresarPerfil}>
        Regresar al perfil
      </button>
    </div>
  );
};

export default RegistroServicios;
