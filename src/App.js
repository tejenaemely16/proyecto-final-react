import React, { useState } from 'react';
import { Index2 } from './pages/index2';
import { Login } from './pages/login1';
import { AdminLogin } from './pages/login2';
import { Registro } from './pages/registro';
import { ServiciosActivos } from './pages/activos';
import { Administrador } from './pages/administrador';
import { RegistroServicios } from './pages/inscripciones';
import { UserProfile } from './pages/perfil';
import { Navegacion } from './pages/planes';
import { GymComponent } from './pages/sedes';
import './App.css'


function App() {
  const [currentForm, setCurrentForm] = useState('index');
  const toggleForm = (formName) => {
    setCurrentForm(formName);
  };

  const components = {
    index: <Index2 onFormSwitch={toggleForm} />,
    login1: <Login onFormSwitch={toggleForm} />,
    login2: <AdminLogin onFormSwitch={toggleForm} />,
    registro: < loginRegistro onFormSwitch={toggleForm} />,
    activos: <ServiciosActivos onFormSwitch={toggleForm} />,
    administrador: <Administrador onFormSwitch={toggleForm} />,
    inscripciones: <RegistroServicios onFormSwitch={toggleForm} />,
    perfil: <UserProfile onFormSwitch={toggleForm} />,
    planes: <Navegacion onFormSwitch={toggleForm} />,
    sedes: <GymComponent onFormSwitch={toggleForm} />,
  };

  const CurrentComponent = components[currentForm];

  return (
    <div className="App">
      {CurrentComponent}
    </div>
  );
}

export default App;
